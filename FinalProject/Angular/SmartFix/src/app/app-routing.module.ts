import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupTenantComponent } from './basic/components/signup-tenant/signup-tenant.component';
import { SignupOwnerComponent } from './basic/components/signup-owner/signup-owner.component';
import { LoginComponent } from './basic/components/login/login.component';
import { SignupComponent } from './basic/components/signup/signup.component';

const routes: Routes = [
  { path:'register_tenant', component:SignupTenantComponent},
  { path:'register_owner', component:SignupOwnerComponent},
  { path:'login', component:LoginComponent},
  { path:'register', component:SignupComponent},
  { path: 'owner', loadChildren: () => import('./owner/owner.module').then(m => m.OwnerModule) },
  { path: 'tenant', loadChildren: () => import('./tenant/tenant.module').then(m => m.TenantModule)}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
