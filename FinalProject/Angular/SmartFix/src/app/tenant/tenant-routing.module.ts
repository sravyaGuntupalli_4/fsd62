import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TenantComponent } from './tenant.component';
import { TenantDashboardComponent } from './pages/tenant-dashboard/tenant-dashboard.component';

const routes: Routes = [
  { path: '', component: TenantComponent },
  { path: 'dashboard', component:TenantDashboardComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TenantRoutingModule { }
