import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TenantRoutingModule } from './tenant-routing.module';
import { TenantComponent } from './tenant.component';
import { TenantDashboardComponent } from './pages/tenant-dashboard/tenant-dashboard.component';


@NgModule({
  declarations: [
    TenantComponent,
    TenantDashboardComponent
  ],
  imports: [
    CommonModule,
    TenantRoutingModule
  ]
})
export class TenantModule { }
