import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OwnerRoutingModule } from './owner-routing.module';
import { OwnerComponent } from './owner.component';
import { OwnerDashboardComponent } from './pages/owner-dashboard/owner-dashboard.component';


@NgModule({
  declarations: [
    OwnerComponent,
    OwnerDashboardComponent
  ],
  imports: [
    CommonModule,
    OwnerRoutingModule
  ]
})
export class OwnerModule { }
