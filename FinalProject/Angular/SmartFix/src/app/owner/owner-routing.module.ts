import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OwnerComponent } from './owner.component';
import { OwnerDashboardComponent } from './pages/owner-dashboard/owner-dashboard.component';

const routes: Routes = [
  { path: '', component: OwnerComponent },
  { path: 'dashboard', component:OwnerDashboardComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnerRoutingModule { }
