import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TenantStorageService } from './basic/services/storage/tenant-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'SmartFix';

  isTenantLoggedIn: boolean = false;
  isOwnerLoggedIn: boolean = false;

  constructor(private router: Router, private tenantStorageService: TenantStorageService) {
    this.isTenantLoggedIn = this.tenantStorageService.isTenantLoggedIn();
    this.isOwnerLoggedIn = this.tenantStorageService.isOwnerLoggedIn();
  }

  ngOnInit() {}

  logout() {
    this.tenantStorageService.signOut();
    this.router.navigateByUrl('login');
  }
}
