import { Injectable } from '@angular/core';

const TOKEN_KEY = 's_token';
const USER_KEY = 's_user';

@Injectable({
  providedIn: 'root'
})
export class TenantStorageService {
  static isTenantLoggedIn() {
    throw new Error('Method not implemented.');
  }
  static isOwnerLoggedIn() {
    throw new Error('Method not implemented.');
  }

  constructor() { }

  saveToken(token: string): void {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem(TOKEN_KEY);
      localStorage.setItem(TOKEN_KEY, token);
    }
  }

  getToken(): string | null {
    if (typeof localStorage !== 'undefined') {
      return localStorage.getItem(TOKEN_KEY);
    }
    return null;
  }

  saveUser(user: any): void {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem(USER_KEY);
      localStorage.setItem(USER_KEY, JSON.stringify(user));
    }
  }

  getUser(): any | null {
    if (typeof localStorage !== 'undefined') {
      const userStr = localStorage.getItem(USER_KEY);
      try {
        return JSON.parse(userStr || '');
      } catch (error) {
        console.error('Error parsing user data:', error);
        return null;
      }
    }
    return null;
  }

  getUserId(): string {
    const user = this.getUser();
    return user ? user.userId || '' : '';
  }

  getUserRole(): string {
    const user = this.getUser();
    return user ? user.role || '' : '';
  }

  isTenantLoggedIn(): boolean {
    const token = this.getToken();
    if (!token) return false;
    const role = this.getUserRole();
    return role === 'TENANT';
  }

  isOwnerLoggedIn(): boolean {
    const token = this.getToken();
    if (!token) return false;
    const role = this.getUserRole();
    return role === 'OWNER';
  }

  signOut(): void {
    if (typeof localStorage !== 'undefined') {
      localStorage.removeItem(TOKEN_KEY);
      localStorage.removeItem(USER_KEY);
    }
  }
}
