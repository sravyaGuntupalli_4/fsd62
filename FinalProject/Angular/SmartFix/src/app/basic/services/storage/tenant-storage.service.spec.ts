import { TestBed } from '@angular/core/testing';

import { TenantStorageService } from './tenant-storage.service';

describe('TenantStorageService', () => {
  let service: TenantStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TenantStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
