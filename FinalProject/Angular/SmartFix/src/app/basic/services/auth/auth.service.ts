import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { TenantStorageService } from '../storage/tenant-storage.service';

const BASIC_URL = 'http://localhost:8080/';
export const AUTH_HEADER = 'authorization';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private tenantStorageService: TenantStorageService) { }

  registerTenant(signUpRequestDTO : any ):Observable<any>{
    return this.http.post(BASIC_URL + "tenant/sign-up",signUpRequestDTO);
  }

  
  registerOwner(signUpRequestDTO : any ):Observable<any>{
    return this.http.post(BASIC_URL + "owner/sign-up",signUpRequestDTO);
  }
  
  login(username:string, password:string){
    return this.http.post(BASIC_URL + "authenticate",{ username , password},{observe: 'response'})
    .pipe(
        map((res: HttpResponse<any>) =>{
          console.log(res.body)
          this.tenantStorageService.saveUser(res.body);
          const tokenLength = res.headers.get(AUTH_HEADER)?.length;
          const bearerToken = res.headers.get(AUTH_HEADER)?.substring(7,tokenLength);
          console.log(bearerToken);
          this.tenantStorageService.saveToken(bearerToken);
          return res;
        })
    );
  }


}
