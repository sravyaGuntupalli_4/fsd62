import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { TenantStorageService } from '../../services/storage/tenant-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  validateForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private notification: NzNotificationService,
    private router: Router,
    private tenantStorageService: TenantStorageService // Injecting TenantStorageService
  ) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  submitForm(): void {
    this.authService.login(
      this.validateForm.get('userName')!.value,
      this.validateForm.get('password')!.value
    ).subscribe(res => {
      console.log(res);

      if (this.tenantStorageService.isTenantLoggedIn()) {
        this.router.navigateByUrl('tenant/dashboard');
      } else if (this.tenantStorageService.isOwnerLoggedIn()) {
        this.router.navigateByUrl('owner/dashboard');
      }
    }, error => {
      this.notification.error(
        'ERROR',
        'Bad credentials',
        { nzDuration: 5000 }
      );
    });
  }
}
