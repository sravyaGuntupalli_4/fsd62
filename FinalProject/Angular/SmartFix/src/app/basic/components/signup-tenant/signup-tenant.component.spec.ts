import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupTenantComponent } from './signup-tenant.component';

describe('SignupTenantComponent', () => {
  let component: SignupTenantComponent;
  let fixture: ComponentFixture<SignupTenantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SignupTenantComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(SignupTenantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
