import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup-tenant',
  templateUrl: './signup-tenant.component.html',
  styleUrls: ['./signup-tenant.component.scss']
})
export class SignupTenantComponent implements OnInit {
  

  validateForm!: FormGroup;

  constructor(private fb: FormBuilder, 
              private authService: AuthService,
              private notification: NzNotificationService,
              private router: Router) {}

  ngOnInit() {
    this.validateForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      checkPassword: ['', [Validators.required, this.confirmationValidator]]
    });
  }

  submitForm(){
      this.authService.registerTenant(this.validateForm.value).subscribe(res =>{
         this.notification
         .success(
          'SUCCESS',
          `Signup Successful`,
          {nzDuration:5000}
         );
         this.router.navigateByUrl('/login');
      },error =>{
        this.notification
        .error(
          'ERROR',
          `${error.error}`,
          {nzDuration:5000}
        )
      });
     }

  // Validator for confirming password
 // Validator for confirming password
confirmationValidator = (control: FormControl): { [s: string]: boolean } => {
  if (!control.value) {
    return { required: true };
  } else if (control.value !== this.validateForm.controls['password'].value) {
    return { confirm: true, error: true };
  }
  return {};
};


  
}
