package com.codeElevate.SmartFix.repository;

import com.codeElevate.SmartFix.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmail(String email);
    User findFirstByEmail(String email);
}
