package com.codeElevate.SmartFix.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.codeElevate.SmartFix.dto.AuthenticationRequest;
import com.codeElevate.SmartFix.dto.SignUpRequestDTO;
import com.codeElevate.SmartFix.dto.UserDto;
import com.codeElevate.SmartFix.entity.User;
import com.codeElevate.SmartFix.repository.UserRepository;
import com.codeElevate.SmartFix.services.authentication.AuthService;
import com.codeElevate.SmartFix.services.jwt.UserDetailsServiceImpl;
import com.codeElevate.SmartFix.util.JwtUtil;

import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

@RestController
public class AuthenticationController {

    @Autowired
    private AuthService authService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired 
    private UserDetailsServiceImpl userDetailsService;

    @Autowired 
    private JwtUtil jwtUtil;

    @Autowired 
    private UserRepository userRepository;

    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/tenant/sign-up")
    public ResponseEntity<?> signUpTenant(@RequestBody SignUpRequestDTO signUpRequestDTO) {
        if (authService.presentByEmail(signUpRequestDTO.getEmail())) {
            return new ResponseEntity<>("Tenant already exists with this email", HttpStatus.NOT_ACCEPTABLE);
        }
        UserDto createdUser = authService.signupTenant(signUpRequestDTO);
        return new ResponseEntity<>(createdUser, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/owner/sign-up")
    public ResponseEntity<?> signUpOwner(@RequestBody SignUpRequestDTO signUpRequestDTO) {
        if (authService.presentByEmail(signUpRequestDTO.getEmail())) {
            return new ResponseEntity<>("Owner already exists with this email", HttpStatus.NOT_ACCEPTABLE);
        }
        UserDto createdUser = authService.signupOwner(signUpRequestDTO);
        return new ResponseEntity<>(createdUser, HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping("/authenticate")
    public void createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest, HttpServletResponse response) throws IOException, JSONException {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                    authenticationRequest.getUsername(), authenticationRequest.getPassword()
            ));
        } catch (BadCredentialsException e) {
            throw new BadCredentialsException("Incorrect username or password");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = jwtUtil.generateToken(userDetails.getUsername());
        User user = userRepository.findFirstByEmail(authenticationRequest.getUsername());

        response.getWriter().write(new JSONObject()
                .put("userId", user.getId())
                .put("role", user.getRole())
                .toString()
        );

        response.addHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader("Access-Control-Allow-Headers", "Authorization, X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept, X-Custom-header");
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + jwt);
    }
}
