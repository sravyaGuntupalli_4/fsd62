package com.codeElevate.SmartFix.services.authentication;

import com.codeElevate.SmartFix.dto.SignUpRequestDTO;
import com.codeElevate.SmartFix.dto.UserDto;
import com.codeElevate.SmartFix.entity.User;
import com.codeElevate.SmartFix.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public AuthServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDto signupTenant(SignUpRequestDTO signUpRequestDTO) {
        User user = createUserFromSignUpRequest(signUpRequestDTO, "TENANT");
        return saveUserAndCreateUserDto(user);
    }

    @Override
    public UserDto signupOwner(SignUpRequestDTO signUpRequestDTO) {
        User user = createUserFromSignUpRequest(signUpRequestDTO, "OWNER");
        return saveUserAndCreateUserDto(user);
    }

    @Override
    public Boolean presentByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    private User createUserFromSignUpRequest(SignUpRequestDTO signUpRequestDTO, String role) {
        validateSignUpRequest(signUpRequestDTO);
        User user = new User();
        user.setEmail(signUpRequestDTO.getEmail());
        user.setName(signUpRequestDTO.getName());
        user.setLastname(signUpRequestDTO.getLastname());
        user.setPhone(signUpRequestDTO.getPhone());
        user.setRole(role);
        user.setPassword(passwordEncoder.encode(signUpRequestDTO.getPassword())); // Encoding password
        return user;
    }

    private void validateSignUpRequest(SignUpRequestDTO signUpRequestDTO) {
        if (signUpRequestDTO == null || signUpRequestDTO.getEmail() == null || signUpRequestDTO.getPassword() == null) {
            throw new IllegalArgumentException("SignUpRequestDTO must not be null, and email/password must not be null.");
        }
    }

    private UserDto saveUserAndCreateUserDto(User user) {
        userRepository.save(user);
        return new UserDto(user); // Assuming UserDto has a constructor that takes a User entity
    }
}
