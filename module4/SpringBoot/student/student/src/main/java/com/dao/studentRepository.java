package com.dao;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.model.student;

@Repository
public interface studentRepository extends JpaRepository<student, Integer> {

	@Query("from student where studName = :sName")
	List<student> findByName(@Param("sName") String studName);

}