package com.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity	//Making the Product class as an Entity Class which will be created as a table in the database.
public class student {

	//@Id makes the column as an primary key column
	//@GeneratedValue make the prodId value to be generated automatically (auto_increment), 
	//@GeneratedValue(strategy=GenerationType.AUTO)
	//@Column will provide the column name to be provided when the table is created

	@Id@GeneratedValue

	private int studId;
	@Column(name="studName")
	private String studName;
	private String course;
	private int fees;
	private String emailId;
	private String password;

	public student() {
	}

	public student(int studId, String studName, String course, int fees, String emailId, String password) {
		this.studId = studId;
		this.studName =studName;
		this.course = course;
		this.fees = fees;
		this.emailId =  emailId;
		this.password =  password;

	}

	public int getStudId() {
		return studId;
	}

	public void setStudId(int studId) {
		this.studId = studId;
	}

	public String getStudName() {
		return studName;
	}

	public void setStudName(String studName) {
		this.studName = studName;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public int getFees() {
		return fees;
	}

	public void setFees(int fees) {
		this.fees = fees;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "student [studId=" + studId + ", studName=" + studName + ", course=" + course + ", fees=" + fees
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}

	
}
