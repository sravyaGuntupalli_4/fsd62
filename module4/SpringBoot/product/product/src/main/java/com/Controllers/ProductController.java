package com.Controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.productDao;
import com.Model.product;

@RestController
public class ProductController {

	@Autowired		//Implementing Dependency Injection
	productDao productDao;
	
	@GetMapping("getAllProducts")
	public List<product> getAllProducts() {		
		return productDao.getAllProducts();	
	}
	
	@GetMapping("getProductById/{id}")
	public product getProductById(@PathVariable("id") int prodId) {
		return productDao.getProductById(prodId);
	}
	
	@GetMapping("getProductByName/{pname}")
	public List<product> getProductByName(@PathVariable("pname") String prodName) {
		return productDao.getProductByName(prodName);
	}
	
	@PostMapping("addProduct")
	public product addProduct(@RequestBody product product) {
		return productDao.addProduct(product);
	}
	
	@PutMapping("updateProduct")
	public product updateProduct(@RequestBody product product) {
		return productDao.updateProduct(product);
	}
	
	@DeleteMapping("deleteProductById/{id}")
	public String deleteProductById(@PathVariable("id") int productId) {
		productDao.deleteProductById(productId);
		return "Product with ProductId: " + productId + ", Deleted Successfully!!!";
	}
	
}

