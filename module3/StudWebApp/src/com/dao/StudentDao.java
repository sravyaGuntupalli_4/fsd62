package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDao {

	public Student stuLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQry = "select * from student where emailId = ? and password = ?";
		
		try {
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Student stu = new Student();
				
				stu.setstuId(rs.getInt(1));
				stu.setstuName(rs.getString(2));
				stu.setmarks(rs.getInt("marks"));
				stu.setGender(rs.getString(4));
				stu.setEmailId(rs.getString(5));
				stu.setPassword(rs.getString(6));
				
				return stu;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public int stuRegister(Student stu) {
		PreparedStatement pst = null;
		Connection con = DbConnection.getConnection();
		
		String insertQry = "insert into student " + 
		"(stuName,marks, gender, emailId, password) values (?, ?, ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQry);
			
			pst.setString(1, stu.getstuName());
			pst.setDouble(2, stu.getmarks());
			pst.setString(3, stu.getGender());
			pst.setString(4, stu.getEmailId());
			pst.setString(5, stu.getPassword());
			
			return pst.executeUpdate();	//returns 1 or 0
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {			
			
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

	
}


