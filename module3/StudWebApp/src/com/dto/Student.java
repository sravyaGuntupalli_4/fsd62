package com.dto;

public class Student {
	
	private int stuId;
	private String stuName;
	private int marks;
	private String gender;
	private String emailId;
	private String password;
	
	public Student() {
	}

	public Student(int stuId, String stuName, int marks, String gender, String emailId, String password) {
		this.stuId = stuId;
		this.stuName = stuName;
		this.marks = marks;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getstuId() {
		return stuId;
	}

	public void setstuId(int stuId) {
		this.stuId = stuId;
	}

	public String getstuName() {
		return stuName;
	}

	public void setstuName(String stuName) {
		this.stuName = stuName;
	}

	public double getmarks() {
		return marks;
	}

	public void setmarks(int marks) {
		this.marks = marks;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [stuId=" + stuId + ", stuName=" + stuName + ", marks=" + marks + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}